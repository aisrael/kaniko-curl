FROM gcr.io/kaniko-project/executor:debug-v0.15.0
COPY --from=odise/busybox-curl /lib /lib
COPY --from=odise/busybox-curl /lib64 /lib64
COPY --from=odise/busybox-curl /usr/lib /usr/lib
COPY --from=odise/busybox-curl /usr/lib64 /usr/lib64
COPY --from=odise/busybox-curl /usr/bin /usr/bin
ENV CURL_CA_BUNDLE=/kaniko/ssl/certs/ca-certificates.crt
ENV PATH /usr/local/bin:/kaniko:/busybox:/usr/bin
