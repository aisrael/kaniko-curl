.PHONY: docker

kaniko_version = v0.15.0

head = $(shell git rev-parse HEAD)

docker:
	export HASH=$(head); \
	docker build -t registry.gitlab.com/aisrael/kaniko-curl:$${HASH:0:8} \
	-t registry.gitlab.com/aisrael/kaniko-curl:debug-$${HASH:0:8} \
	-t registry.gitlab.com/aisrael/kaniko-curl:latest .
